#include "HTTPHandler.h"

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

HTTPHandler::HTTPHandler()  {
    curl = curl_easy_init();
    if (curl) {
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	}
}

HTTPHandler::~HTTPHandler() {
	curl_easy_cleanup(curl);
}

bool HTTPHandler::SendRequest(std::string *RequestURL) {

	if (curl) {
		CURLcode res;
		curl_easy_setopt(curl, CURLOPT_URL, RequestURL->c_str());
//		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "key=obswx15r");
	    res = curl_easy_perform(curl);
	    

	    //std::cout << readBuffer << std::endl;
	    return true;
	}

	return false;
}

std::string HTTPHandler::ReadBuffer() {
	std::string tempBuffer = readBuffer;
	readBuffer = "";
	return tempBuffer;
}