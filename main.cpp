#include <iostream>
#include <string>
#include <stdio.h>
#include <sstream>
#include <cstring>
#include <list>
#include <thread>
#include <vector>
#include <iterator>
#include <ctime>
#include <fstream>
#include <algorithm>
#include <streambuf>
#include "Crawler.h"

#include "AthleteBio.h"
#include "HTTPHandler.h"

int main(void)
{
	std::string outputFile = "out.csv";
	time_t now = time(0);
   	char* dt = ctime(&now);
   	std::cout << "The local date and time is: " << dt << std::endl;

	int maxThreads = std::thread::hardware_concurrency() * 2;

	for (int division = 1; division < 3; division++)
	{
		std::cout << "division: " << division << std::endl;
		if (division != 11)
		{
			Crawler mCrawler(1, 2, division);
			int totalPageCount = mCrawler.GetTotalPages();

			std::list < Crawler * > crawlerList;
			std::vector< std::thread > threads;
			int start = 0;

			while ( start < totalPageCount )
			{
				int upperLimit = start + totalPageCount / maxThreads;
				if (upperLimit > totalPageCount)
					upperLimit = totalPageCount;

				Crawler *nCrawler = new Crawler( start + 1, upperLimit, division );
				crawlerList.push_back( nCrawler );

				start = totalPageCount / maxThreads + start;
			}

			std::list< Crawler *>::iterator it;
			for ( it = crawlerList.begin(); it != crawlerList.end(); it++ )
				threads.push_back( std::thread( &Crawler::StartWork, *it ) );

			std::vector< std::thread>::iterator vIt;
			for ( vIt = threads.begin(); vIt != threads.end(); vIt++)
			{
				vIt->join();
			}

			for ( it = crawlerList.begin(); it != crawlerList.end(); it++ )
			{
				Crawler *nCrawler = *it;
				//std::cout << nCrawler->GetLimit() << std::endl;
				std::stringstream ss;
				ss << nCrawler->GetLimit();

				std::string fileName = std::string( ss.str() + ".txt" );

				std::ifstream t( fileName );
				std::string str;
				std::ofstream mFile;
				
				t.seekg(0, std::ios::end);   
				str.reserve(t.tellg());
				t.seekg(0, std::ios::beg);

				str.assign((std::istreambuf_iterator<char>(t)),
				            std::istreambuf_iterator<char>());
				
				mFile.open( outputFile.c_str(), std::ofstream::out | std::ofstream::app );

				mFile << str;

				mFile.close();

				std::remove( fileName.c_str() );

				delete nCrawler;
			}

			threads.clear();
			crawlerList.clear();
		}
	}

	now = time(0);
   
   // convert now to string form
   dt = ctime(&now);

   std:: cout << "The local date and time is: " << dt << std::endl;\
	std::ifstream inFile( outputFile ); 
	std::cout << "Athletes: " << std::count(std::istreambuf_iterator<char>(inFile), 
		std::istreambuf_iterator<char>(), '\n') << std::endl;

}
