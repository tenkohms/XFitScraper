#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>

#include "Crawler.h"
#include "HTTPHandler.h"

void print_json_object( json_t *element ) {
    size_t size;
    const char *key;
    json_t *value;

    size = json_object_size( element );

    json_object_foreach( element, key, value) {
    	std::cout << key << std::endl;
    }
}

Crawler::Crawler(unsigned int StartCount, unsigned int Limit, int Division) 
	: currentCount_( StartCount )
	, limit_( Limit )
	, division_( Division )
{
}

Crawler::~Crawler() {
	delete root_;
}

void Crawler::StartWork()
{
	std::stringstream limitCount;
	limitCount << limit_;
	while( currentCount_ <= limit_ )
	{
		std::stringstream ss, dd;
		ss << currentCount_;
		dd << division_;
		std::string fReq = std::string( "https://games.crossfit.com/competitions/api/v1/competitions/open/2017/leaderboards?page=" + ss.str() + "&competition=1&year=2017&division=" + dd.str() + "&scaled=0&sort=0&fittest=1&fittest1=0&occupation=0" );
		
		HTTPHandler networkHandler;
		if ( !networkHandler.SendRequest( &fReq ) )
		{
			std::cout << "ERROR 3001: " << currentCount_ << " :: " << limit_ << std::endl;
			break;
		}

		std::string readBuffer = networkHandler.ReadBuffer();
		LoadJSON( readBuffer.c_str() );
		WriteAthletesToFile( std::string ( limitCount.str() + ".txt" ) );
		currentCount_++;
	}
}

json_t *Crawler::load_json(const char * text) {
	json_t *root;
	json_error_t error;

	root = json_loads(text, 0 , &error);

	if (root) {
		return root;
	}
	else {
		std::cout << "ERROR ::load_json" << std::endl;
		return (json_t *) 0;
	}
}

std::string Crawler::GetBenchMarks( std::string AthletePage )
{
	std::string scores_ = "";
	if ( AthletePage.find("table class=\"stats\"") != std::string::npos )
	{

	while ( AthletePage.find("table class=\"stats\"") != std::string::npos )
	{
		AthletePage = AthletePage.substr( AthletePage.find("table class=\"stats\"") + 1 );

		for (int i = 0; i < 3; i++)
		{
			std::string statData = AthletePage.substr( AthletePage.find( "<td>" ) + 4, AthletePage.find("</td>") - AthletePage.find( "<td>" ) - 4);
			boost::trim(statData);
			if (statData == "" )
				statData = "--";
			scores_.append(statData + ",");
			AthletePage = AthletePage.substr( AthletePage.find( "</td>") + 1);
		}
	} 

	scores_.pop_back();

	return scores_;
	}
	else
	{
		return "--,--,--,--,--,--,--,--,--,--,--,--";
	}
}

bool Crawler::LoadJSON( const char * text )
{
	json_t * nRoot = load_json( text );

	if (nRoot) {
		root_ = nRoot;
		//print_json_object( root_ );
		return true;
	}

	return false;
}

int Crawler::GetTotalPages()
{
	int totalPages = -1;

	std::stringstream ss, dd;
	ss << currentCount_;
	dd << division_;
	std::string fReq = std::string( "https://games.crossfit.com/competitions/api/v1/competitions/open/2017/leaderboards?page=" + ss.str() + "&competition=1&year=2017&division=" + dd.str() + "&scaled=0&sort=0&fittest=1&fittest1=0&occupation=0" );
	
	HTTPHandler networkHandler;
	if ( !networkHandler.SendRequest( &fReq ) )
	{
		std::cout << "ERROR 4001: " << currentCount_ << " :: " << limit_ << std::endl;
		return -1;
	}

	std::string readBuffer = networkHandler.ReadBuffer();
	LoadJSON( readBuffer.c_str() );

	if (root_)
	{
		json_t *data;
		data = json_object_get(root_, "totalpages");
		totalPages = json_integer_value( data );
	}

	return totalPages;
}

int Crawler::CurrentPage()
{
	int currentPage = 0;

	if (root_)
	{
		json_t *data;
		data = json_object_get(root_, "currentpage");
		currentPage = json_integer_value( data );
	}

	return currentPage;
}

int Crawler::GetLimit()
{
	return (int)limit_;
}

void Crawler::WriteAthletesToFile(std::string FileName)
{
	json_t * athleteArray;
	athleteArray = json_object_get( root_, "athletes" );

	if ( !json_is_array( athleteArray ) )
	{
		std::cout << "Error parsting athlete array..." << std::endl;
		return;
	}
	/*
	"userid": "107150",
	"name": "Haydn Wolfsbauer",
	"regionid": "3",
	"affiliateid": "14343",
	"divisionid": "1",
	"highlight": 0,
	"age": 30,
	"region": "Australia",
	"height": "182 cm",
	"weight": "92 kg",
	"profilepic": "https:\/\/profilepicsbucket.crossfit.com\/e6eff-P107150_1-184.jpg",
	"overallrank": "101",
	"overallscore": "329",
	"affiliate": "",
	"division": 1,
	*/
	for ( unsigned int currentAthleteIndex = 0; currentAthleteIndex < json_array_size( athleteArray ); currentAthleteIndex++ )
	{
		json_t  * currentAthlete, * athleteID, * athleteName, * regionID, * affiliateID,
				* divisionid, * age, * regionString, * height, * weight, * pic,
				* overallrank, * overallscore, * divisionInt, * scoreArray;
		currentAthlete = json_array_get( athleteArray, currentAthleteIndex );
		if ( !json_is_object( currentAthlete) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		athleteID = json_object_get( currentAthlete, "userid" );
		if ( !json_is_string( athleteID) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}
		// std::string BenchmarkData;
		// HTTPHandler mHandler;
		// std::string req = std::string( "https://games.crossfit.com/athlete/" + std::string( json_string_value( athleteID ) ) );
		// if ( mHandler.SendRequest( &req) ) {

		// 	std::string data = mHandler.ReadBuffer();
		// 	BenchmarkData = GetBenchMarks(data);
		// }

		athleteName = json_object_get( currentAthlete, "name" );
		if ( !json_is_string( athleteName) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		regionID = json_object_get( currentAthlete, "regionid" );
		if ( !json_is_string( regionID) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		affiliateID = json_object_get( currentAthlete, "affiliateid" );
		if ( !json_is_string( affiliateID) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		divisionid = json_object_get( currentAthlete, "divisionid" );
		if ( !json_is_string( divisionid) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		age = json_object_get( currentAthlete, "age" );
		if ( !json_is_integer( age) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		regionString = json_object_get( currentAthlete, "region" );
		if ( !json_is_string( regionString ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		height = json_object_get( currentAthlete, "height" );
		if ( !json_is_string( height ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		weight = json_object_get( currentAthlete, "weight" );
		if ( !json_is_string( weight ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		pic = json_object_get( currentAthlete, "profilepic" );
		if ( !json_is_string( pic ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		overallrank = json_object_get( currentAthlete, "overallrank" );
		if ( !json_is_string( overallrank ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		overallscore = json_object_get( currentAthlete, "overallscore" );
		if ( !json_is_string( overallscore ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		divisionInt = json_object_get( currentAthlete, "division" );
		if ( !json_is_integer( divisionInt ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		scoreArray = json_object_get( currentAthlete, "scores");
		if ( !json_is_array( scoreArray ) )
		{
			std::cout << "Error 1427; breaking." << std::endl;
			break;
		}

		std::string scores;
		for ( unsigned int workout = 0; workout < json_array_size( scoreArray ); workout++ )
		{
			json_t *currentWorkout = json_array_get( scoreArray, workout );
			json_t *workoutRank, * scoreDisplay, *scoreDetails, *tieBreak;

			workoutRank = json_object_get( currentWorkout, "workoutrank" );
			scoreDisplay = json_object_get( currentWorkout, "scoredisplay" );
			scoreDetails = json_object_get( currentWorkout, "scoredetails" );
			tieBreak = json_object_get( scoreDetails, "time" );
			std::stringstream ss;
			ss << json_integer_value( tieBreak );
			scores.append( std::string( std::string( json_string_value( workoutRank ) ) + ',' + std::string( json_string_value( scoreDisplay ) ) + "," + ss.str() ) );
			if (workout < json_array_size ( scoreArray ) - 1)
				scores.append( "," );
		}

		std::ofstream mFile;
		mFile.open( FileName.c_str(), std::ofstream::out | std::ofstream::app );

		mFile << std::string( json_string_value( athleteID) ) << "," << std::string( json_string_value( athleteName ) ) << ","
			  << std::string( json_string_value( regionID) ) << "," << std::string( json_string_value( affiliateID ) ) << ","
			  << std::string( json_string_value( divisionid) ) << "," << json_integer_value( age ) << ","
			  << std::string( json_string_value( regionString) ) << "," << std::string( json_string_value( height ) ) << ","
			  << std::string( json_string_value( weight) ) << "," << std::string( json_string_value( pic ) ) << ","
			  << std::string( json_string_value( overallrank) ) << "," << std::string( json_string_value( overallscore ) ) << ","
			  << json_integer_value( divisionInt) << "," << scores << std::endl;

		mFile.close();
		
	}
}