CC = g++
CFLAGS = -g -std=gnu++11 -ljansson -L/usr/lib/x86_64 -lcurl
LIBFLAGS = -c -std=gnu++11 -ljansson -L/usr/lib/x86_64 -lcurl

default: clean all

all: HTTPHandler Crawler Main

Main:
	$(CC) main.cpp -pthread -L. -lHTTPHandler -lCrawler $(CFLAGS)

HTTPHandler:
	$(CC) HTTPHandler.cpp -o libHTTPHandler.so $(LIBFLAGS)

Crawler:
	$(CC) Crawler.cpp -o libCrawler.so -L. -lHTTPHandler $(LIBFLAGS)

clean:
	rm -rf a.out
	rm -rf libHTTPHandler.so
	rm -rf libCrawler.so
