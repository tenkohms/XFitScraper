#include <iostream>
#include <curl/curl.h>
#include <cstring>

class HTTPHandler {
public:
  HTTPHandler();

  ~HTTPHandler();

  bool SendRequest(std::string *RequestURL);

  std::string ReadBuffer();

private:
  CURL *curl;
  std::string readBuffer;
};