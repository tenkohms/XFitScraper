#include <jansson.h>

class Crawler {
public:
	Crawler(unsigned int StartCount, unsigned int Limit, int Division);
	~Crawler();
	bool LoadJSON(const char *);
	int GetTotalPages();
	int CurrentPage();
	int GetLimit();
	void WriteAthletesToFile(std::string FileName);

	void StartWork();

private:
	//private member functions	
	json_t *load_json(const char *);
	std::string GetBenchMarks(std::string AthletePage);

	//private vars
	json_t * root_;

	unsigned int currentCount_, limit_;
	int division_;
};
